# README #
Opencart 2.X ve üzeri sürümlerde çalışıan Bu modül Ürün eklenirken, vergi sınıfı seçildiğinde, girilmiş olan fiyatı 
bu vergi sınıfının oranına göre input alanında günceller. 
Bu sayede ürünlerinizin son fiyatını yazabilir ve ham fiyatları ile kaydedebilirsiniz.


### How do I get set up? ###

OcMod modüldür. 
1 - Admin panelinizdeki eklentiler menüsünden eklenti yükle sayfasına gidin.
2 - Eklentiyi yükledikten sonra eklentiler menüsünden modifikasyonlar sayfasına gidin ve modifikasyonları tazeleyin.
Modül Ürün ekleme - düzenleme sayfasında işlem yapabiir duuma gelecektir.
